
![logo](https://www.oregon-core.net/images/logo-github.png) Welcome to OregonCore!
=================================================================================

OregonCore is a piece of software that targets emulation of a World of Warcraft: The Burning Crusade game server. Our goal is to create a stable MMO framework and to help teach and learn development among our community. OregonCore has sustained itself with its tight knit community of developers, testers and bug reporters and thanks all those who have been involved with the project over the years.

Build Status
------------

| Compiler      | Platform    | Branch | Status                  |
|:--------------|:------------|:------:|:-----------------------:|
| clang         | Linux x64   | master | [![Build Status][1]][7] |


Docs, Community and Support
---------------------------

Development went ahead and documentation was left behind. Although we try to keep stuff documented, there's a lot to do.
Here's what we've saved from old [wiki][3] and our growing new official [documentation][4] here.

Next place is our [Discord][5], you can get support there as well as be a part of our growing community.
We have also a [forum][6], this is a place for developers to hang out and discuss all things.

If you have found a bug you may report it on our bugtracker, issues must include revision hash and as many details as possible to aide us in resolving it as quickly as possible.

[1]: https://api.travis-ci.org/talamortis/OregonCore.svg?branch=master


